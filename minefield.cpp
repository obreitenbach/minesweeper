#include "minefield.h"

Minefield::Minefield()
{
    connect(this, &Minefield::mineTriggered, this, &Minefield::lost);
}

void Minefield::startNewGame(int rows, int cols, int cellEdgeLength, int mineProbability)
{
    this->mineCount = 0;
    this->boardSetup = false;
    this->cells.clear();

    this->rows = rows;
    this->cols = cols;
    this->cellEdgeLength = cellEdgeLength;
    this->mineProbability = mineProbability;
    update();
}

void Minefield::pauseGame()
{
    fieldLocked = true;
}

void Minefield::unpauseGame()
{
    fieldLocked = false;
}

void Minefield::setupMinefield(QPainter *painter)
{
    for(int i = 0; i < cols; i++) {
        for(int j = 0; j < rows; j++) {
            Cell *c;
            int rand = Utils::randomNumberBetween(0, 100);
            if(rand <= mineProbability) {
                c = new Cell(cellEdgeLength, i * cellEdgeLength, j * cellEdgeLength, true);
                mineCount++;
            } else {
                c = new Cell(cellEdgeLength, i * cellEdgeLength, j * cellEdgeLength, false);
            }
            c->drawCell(painter);
            cells.push_back(c);
        }
    }
    foreach(Cell *c, cells) {
        fetchNeighborCellData(c);
    }

    markerCount = mineCount;

    emit mineCountChanged();
    emit markerCountChanged();
}

int Minefield::getMineCount()
{
    return mineCount;
}

int Minefield::getMarkerCount()
{
    return markerCount;
}

void Minefield::paintEvent(QPaintEvent *event)
{
    QPainter *p = new QPainter(this);
    if(!boardSetup) {
        setupMinefield(p);
        boardSetup = true;
    } else {
        repaintCells(p);
    }
    delete p;
}

void Minefield::mousePressEvent(QMouseEvent *event)
{
    if(!fieldLocked)  {
        QPoint mousePosition = this->mapFromGlobal(QCursor::pos());
        onCellClicked(mousePosition, event);

        if(isGameWon()) {
            emit gameWon();
        }
    }
}

void Minefield::onCellClicked(QPoint mousePosition, QMouseEvent *event)
{
    if(event->buttons() == Qt::LeftButton) {
        handleLeftClick(mousePosition);
    } else if(event->buttons() == Qt::RightButton) {
        handleRightClick(mousePosition);
    } else if(event->buttons() == Qt::MiddleButton) {
        handleMiddleClick(mousePosition);
    }
}

void Minefield::handleLeftClick(QPoint mousePosition)
{
    Cell *c = getCellCointainingPoint(mousePosition);
    if(c->containsMine()) {
        emit mineTriggered();
    } else {
        if(c->getNeighborMinesCount() == 0) {
            revealZeroFields(c);
        } else {
            c->reveal();
        }
        update();
    }
}

void Minefield::handleRightClick(QPoint mousePosition)
{
    Cell *c = getCellCointainingPoint(mousePosition);
    if(!c->isMarked() && !c->isRevealed() && markerCount > 0) {
        c->mark();
        markerCount--;
    }
    update();
    emit markerCountChanged();
}

void Minefield::handleMiddleClick(QPoint mousePosition)
{
    Cell *c = getCellCointainingPoint(mousePosition);
    if (c->isMarked()) {
        c->unmark();
        markerCount++;
    }
    update();
    emit markerCountChanged();
}

bool Minefield::isGameWon()
{
    int fieldsWithNoMinesCount = (rows * cols) - mineCount;
    int revealedFields = 0;
    foreach (Cell *cell, cells) {
        if(cell->isRevealed() || (cell->isMarked() && !cell->containsMine())) {
            revealedFields++;
        }
    }

    return fieldsWithNoMinesCount == revealedFields;
}

void Minefield::repaintCells(QPainter *painter)
{
    foreach (Cell *cell, cells) {
        cell->drawCell(painter);
    }
}

QVector<Cell*> Minefield::getNeighbourCells(Cell *c)
{
    QVector<Cell*> neighbourCells;
    Cell *cNorth = getNeighbourCell(c, NeighbourCellDir::NORTH);
    Cell *cSouth = getNeighbourCell(c, NeighbourCellDir::SOUTH);
    Cell *cEast = getNeighbourCell(c, NeighbourCellDir::EAST);
    Cell *cWest = getNeighbourCell(c, NeighbourCellDir::WEST);

    Cell *cNorthEast = getNeighbourCell(c, NeighbourCellDir::NORTH_EAST);
    Cell *cSouthEast = getNeighbourCell(c, NeighbourCellDir::SOUTH_EAST);
    Cell *cNorthWest = getNeighbourCell(c, NeighbourCellDir::NORTH_WEST);
    Cell *cSouthWest = getNeighbourCell(c, NeighbourCellDir::SOUTH_WEST);

    neighbourCells << cNorth << cSouth << cEast << cWest
                   << cNorthEast << cNorthWest << cSouthEast << cSouthWest;

    return neighbourCells;
}

Cell *Minefield::getCellCointainingPoint(QPoint p)
{
    foreach (Cell *cell, cells) {
        if(cell->containsPoint(&p)) {
            return cell;
        }
    }
    return new Cell(-1, -1, -1, false);
}

void Minefield::fetchNeighborCellData(Cell *c)
{
    int counter = 0;
    QVector<Cell*> neighbourCells = getNeighbourCells(c);
    foreach(Cell *neighbourCell, neighbourCells) {
        if(neighbourCell->containsMine()) {
            counter++;
        }
    }
    c->setNeighborMinesCount(counter);
}

Cell *Minefield::getNeighbourCell(Cell *c, NeighbourCellDir dir)
{
    switch(dir) {
    case NeighbourCellDir::NORTH:
        return getCellCointainingPoint(QPoint(c->getX() + (0.5 * c->getEdgeLenght()), c->getY() - (0.5 * c->getEdgeLenght())));
    case NeighbourCellDir::SOUTH:
        return getCellCointainingPoint(QPoint(c->getX() + (0.5 * c->getEdgeLenght()), c->getY() + (1.5 * c->getEdgeLenght())));
    case NeighbourCellDir::WEST:
        return  getCellCointainingPoint(QPoint(c->getX() - (0.5 * c->getEdgeLenght()), c->getY() + (0.5 * c->getEdgeLenght())));
    case NeighbourCellDir::EAST:
        return getCellCointainingPoint(QPoint(c->getX() + (1.5 * c->getEdgeLenght()), c->getY() + (0.5 * c->getEdgeLenght())));
    case NeighbourCellDir::NORTH_WEST:
        return getCellCointainingPoint(QPoint(c->getX() - (0.5 * c->getEdgeLenght()), c->getY() - (0.5 * c->getEdgeLenght())));
    case NeighbourCellDir::NORTH_EAST:
        return getCellCointainingPoint(QPoint(c->getX() + (1.5 * c->getEdgeLenght()), c->getY() - (0.5 * c->getEdgeLenght())));
    case NeighbourCellDir::SOUTH_WEST:
        return getCellCointainingPoint(QPoint(c->getX() - (0.5 * c->getEdgeLenght()), c->getY() + (1.5 * c->getEdgeLenght())));
    case NeighbourCellDir::SOUTH_EAST:
        return getCellCointainingPoint(QPoint(c->getX() + (1.5 * c->getEdgeLenght()), c->getY() + (1.5 * c->getEdgeLenght())));
    }
}

void Minefield::revealZeroFields(Cell *c)
{
    if(isCellOnBoard(c)) {
        if(!c->isRevealed()) {
            if(c->getNeighborMinesCount() == 0) {
                c->reveal();
                QVector<Cell*> neighbourCells = getNeighbourCells(c);
                foreach (Cell *cell, neighbourCells) {
                    if(cell->getNeighborMinesCount() == 0) {
                        revealNonZeroNeighbours(cell);
                        revealZeroFields(cell);
                    }
                }
            }
        }
    }
}

void Minefield::revealNonZeroNeighbours(Cell *c)
{
    QVector<Cell*> neighbourCells = getNeighbourCells(c);
    foreach (Cell *neighbourCell, neighbourCells) {
        if(neighbourCell->getNeighborMinesCount() != 0) {
            neighbourCell->reveal();
        }
    }
}

bool Minefield::isCellOnBoard(Cell *c)
{
    return c->getX() >= 0 && c->getX() <= cols * cellEdgeLength &&
            c->getY() >= 0 && c->getY() <= rows * cellEdgeLength;
}

void Minefield::lost()
{
    revealBoard();
    boom();
    emit gameLost();
}

void Minefield::revealBoard()
{
    foreach (Cell *cell, cells) {
        cell->revealBoard();
        cell->reveal();
    }
    update();
}

void Minefield::boom()
{
}
