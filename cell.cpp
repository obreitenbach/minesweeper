#include "cell.h"

Cell::Cell(int edgeLenght, int x, int y, bool hasMine)
{
    this->hasMine = hasMine;
    this->edgeLenght = edgeLenght;
    this->x = x;
    this->y = y;
}

void Cell::drawCell(QPainter *painter)
{
    if(validCell()) {
        QPoint *topLeft = new QPoint(x, y);
        QPoint *bottomRight = new QPoint(x + edgeLenght, y + edgeLenght);
        QRect *cell = new QRect(*topLeft, *bottomRight);
        painter->drawRect(*cell);

        if(boardRevealed) {
            QBrush b;
            b.setStyle(Qt::Dense5Pattern);
            if(hasMine && marked) {
                b.setColor(Qt::blue);
            } else if(!hasMine && marked) {
                b.setColor(Qt::red);
            } else {
                b.setColor(Qt::white);
            }
            painter->setBrush(b);
            QRect rect(x, y, edgeLenght, edgeLenght);
            painter->drawRect(rect);
        }

        if(!isHidden && hasMine) {
            drawMine(painter);
        } else if(!isHidden && !hasMine) {
            if(neighborMinesCount == 0) {
                drawScrambled(painter);
            } else {
                drawNeighbourMinesCount(painter);
            }
        } else if(isHidden && marked) {
            drawMarked(painter);
        }

        delete topLeft;
        delete bottomRight;
        delete cell;
    }
}

void Cell::reveal()
{
    this->isHidden = false;
}

bool Cell::containsMine()
{
    return hasMine;
}

int Cell::getX()
{
    return this->x;
}

int Cell::getY()
{
    return this->y;
}

int Cell::getEdgeLenght()
{
    return this->edgeLenght;
}

int Cell::getNeighborMinesCount()
{
    return neighborMinesCount;
}

void Cell::setNeighborMinesCount(int value)
{
    neighborMinesCount = value;
}

bool Cell::isRevealed()
{
    return !this->isHidden;
}

void Cell::mark()
{
    this->marked = true;
}

void Cell::unmark()
{
    this->marked = false;
}

void Cell::revealBoard()
{
    this->boardRevealed = true;
}

bool Cell::isMarked()
{
    return this->marked;
}

void Cell::drawMine(QPainter *painter)
{
    int horizontalCenter = x + (edgeLenght / 2);
    int verticalCenter   = y + (edgeLenght / 2);
    QPoint *center = new QPoint(horizontalCenter, verticalCenter);
    QBrush oldBrush = painter->brush();
    QBrush b;
    b.setStyle(Qt::SolidPattern);
    painter->setBrush(b);
    painter->drawEllipse(*center, 5, 5);
    painter->setBrush(oldBrush);
}

void Cell::drawScrambled(QPainter *painter)
{
    QBrush oldBrush = painter->brush();
    QBrush b;
    b.setStyle(Qt::Dense5Pattern);
    painter->setBrush(b);
    QRect rect(x, y, edgeLenght, edgeLenght);
    painter->drawRect(rect);
    painter->setBrush(oldBrush);
}

void Cell::drawNeighbourMinesCount(QPainter *painter)
{
    QPoint p(x + edgeLenght / 2, y + (edgeLenght / 2) + edgeLenght / 4);
    QFont font;
    font.setPixelSize(edgeLenght / 2);
    painter->setFont(font);
    painter->drawText(p, QString::number(neighborMinesCount));
}

void Cell::drawMarked(QPainter *painter)
{
    QPoint p(x + edgeLenght / 2, y + (edgeLenght / 2) + edgeLenght / 4);
    QFont font;
    font.setPixelSize(edgeLenght / 2);
    painter->setFont(font);
    QString percent = "%";
    painter->drawText(p, percent);
}

bool Cell::validCell()
{
    return this->x != -1 && this->y != -1;
}

bool Cell::containsPoint(QPoint *p)
{
    int pX = p->x();
    int pY = p->y();

    bool matchingX = pX > this->x && pX < this->x + this->edgeLenght;
    bool matchingY = pY > this->y && pY < this->y + this->edgeLenght;

    return matchingX && matchingY;
}
