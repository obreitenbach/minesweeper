#ifndef CELL_H
#define CELL_H

#include <QPainter>
#include <QRect>
#include <QPoint>
#include <QDebug>

class Cell : public QObject
{
    Q_OBJECT
public:
    Cell(int edgeLenght, int x, int y, bool hasMine);
    void drawCell(QPainter *painter);
    void reveal();
    bool containsPoint(QPoint *p);
    bool containsMine();

    int getX();
    int getY();
    int getEdgeLenght();

    int getNeighborMinesCount();
    void setNeighborMinesCount(int value);
    bool isRevealed();
    bool isMarked();
    void mark();
    void unmark();
    void revealBoard();
private:
    QColor c;
    bool boardRevealed = false;
    bool hasMine;
    bool marked = false;
    bool isHidden = true;
    int edgeLenght;
    int x;
    int y;
    int neighborMinesCount = 0;

    void drawMine(QPainter *painter);
    void drawScrambled(QPainter *painter);
    void drawNeighbourMinesCount(QPainter *painter);
    void drawMarked(QPainter *painter);

    bool validCell();
};

#endif // CELL_H
