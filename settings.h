#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>

namespace Ui {
class Settings;
}

enum Preset {
    EASY,
    MEDIUM,
    HARD
};

class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = 0);
    int getColCount();
    int getRowCount();
    int getMineProb();
    int getMarkerCount();

    ~Settings();

    bool getUserAborted();

private slots:
    void on_btnPresetEasy_clicked();

    void on_btnPresetMedium_clicked();

    void on_btnPresetHard_clicked();

    void on_btnApplySettings_clicked();

    void on_btnAbort_clicked();

private:
    Ui::Settings *ui;

    bool userAborted = false;

    int rowsEasy = 10;
    int colsEasy = 10;
    int mineProbEasy = 5;

    int rowsMedium = 10;
    int colsMedium = 15;
    int mineProbMedium = 10;

    int rowsHard = 20;
    int colsHard = 25;
    int mineProbHard = 15;

    void applyPreset(Preset preset);

    void setTextboxes(int rows, int cols, int mineProb);
};

#endif // SETTINGS_H
