#ifndef UTILS_H
#define UTILS_H

#include <random>

/**
 * @brief Util class.
 */
class Utils
{
public:
    Utils();

    static int randomNumberBetween(int from, int to);
};

#endif // UTILS_H
