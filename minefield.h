#ifndef MINEFIELD_H
#define MINEFIELD_H

#include "cell.h"
#include "utils.h"

#include <QWidget>
#include <QPainter>
#include <QVector>
#include <QDebug>
#include <QMediaPlayer>
#include <QMouseEvent>

enum NeighbourCellDir {
    NORTH,
    SOUTH,
    EAST,
    WEST,
    NORTH_EAST,
    NORTH_WEST,
    SOUTH_EAST,
    SOUTH_WEST
};

enum GameResult {
    WON,
    LOST
};

class Minefield : public QWidget
{
    Q_OBJECT
public:
    Minefield();

    void startNewGame(int rows, int cols, int cellEdgeLength, int mineProbability);
    void pauseGame();
    void unpauseGame();

    void setupMinefield(QPainter *painter);
    int getMineCount();
    int getMarkerCount();

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);

private:
    QString BOOM1_SOUNDEFFECT = "qrc:/soundeffects/boom1.mp3";
    bool boardSetup = false;
    bool fieldLocked = false;
    QVector<Cell*> cells;
    int rows;
    int cols;
    int cellEdgeLength;
    int mineProbability;

    int mineCount = 0;
    int markerCount = 10;

    void onCellClicked(QPoint mousePosition, QMouseEvent *event);
    void repaintCells(QPainter *painter);
    QVector<Cell*> getNeighbourCells(Cell *c);
    Cell *getCellCointainingPoint(QPoint p);
    void fetchNeighborCellData(Cell *c);

    Cell *getNeighbourCell(Cell *c, NeighbourCellDir dir);
    void revealZeroFields(Cell *c);
    void revealNonZeroNeighbours(Cell *c);
    bool isCellOnBoard(Cell *c);

    void handleLeftClick(QPoint mousePosition);
    void handleRightClick(QPoint mousePosition);
    void handleMiddleClick(QPoint mousePosition);

    bool isGameWon();

    void boom();
signals:
    void mineTriggered();
    void mineCountChanged();
    void markerCountChanged();
    void gameLost();
    void gameWon();
public slots:
    void revealBoard();
    void lost();
};

#endif // MINEFIELD_H
