#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
}

int Settings::getColCount()
{
    return ui->txtColCount->text().toInt();
}

int Settings::getRowCount()
{
    return ui->txtRowCount->text().toInt();
}

int Settings::getMineProb()
{
    return ui->txtMineProb->text().toInt();
}

Settings::~Settings()
{
    delete ui;
}

void Settings::applyPreset(Preset preset)
{
    if(preset == Preset::EASY) {
        setTextboxes(rowsEasy, colsEasy, mineProbEasy);
    } else if(preset == Preset::MEDIUM) {
        setTextboxes(rowsMedium, colsMedium, mineProbMedium);
    } else if(preset == Preset::HARD) {
        setTextboxes(rowsHard, colsHard, mineProbHard);
    }
}

void Settings::setTextboxes(int rows, int cols, int mineProb)
{
    ui->txtRowCount->setText(QString::number(rows));
    ui->txtColCount->setText(QString::number(cols));
    ui->txtMineProb->setText(QString::number(mineProb));
}

void Settings::on_btnPresetEasy_clicked()
{
    applyPreset(Preset::EASY);
}

void Settings::on_btnPresetMedium_clicked()
{
    applyPreset(Preset::MEDIUM);
}

void Settings::on_btnPresetHard_clicked()
{
    applyPreset(Preset::HARD);
}

void Settings::on_btnApplySettings_clicked()
{
    close();
}

void Settings::on_btnAbort_clicked()
{
    userAborted = true;
    close();
}

bool Settings::getUserAborted()
{
    return userAborted;
}
