#include "statistics.h"
#include "ui_statistics.h"

Statistics::Statistics(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Statistics)
{
    ui->setupUi(this);
    setWindowTitle("Statistiken");
    ui->tblScores->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

void Statistics::addEntry(QString fieldDim, QString mineProbability, int winCount, int loseCount, QString fastestTimeWon)
{
    int itemCount = ui->tblScores->rowCount();
    itemCount++;
    ui->tblScores->setRowCount(itemCount);
    ui->tblScores->setItem(itemCount - 1, 0, new QTableWidgetItem(fieldDim));
    ui->tblScores->setItem(itemCount - 1, 1, new QTableWidgetItem(mineProbability));
    ui->tblScores->setItem(itemCount - 1, 2, new QTableWidgetItem(QString::number(winCount)));
    ui->tblScores->setItem(itemCount - 1, 3, new QTableWidgetItem(QString::number(loseCount)));
    ui->tblScores->setItem(itemCount - 1, 4, new QTableWidgetItem(fastestTimeWon));
}

Statistics::~Statistics()
{
    delete ui;
}
