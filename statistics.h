#ifndef STATISTICS_H
#define STATISTICS_H

#include <QDialog>

namespace Ui {
class Statistics;
}

class Statistics : public QDialog
{
    Q_OBJECT

public:
    explicit Statistics(QWidget *parent = 0);

    void addEntry(QString fieldDim, QString mineProbability, int winCount, int loseCount, QString fastestTimeWon);

    ~Statistics();

private:
    Ui::Statistics *ui;
};

#endif // STATISTICS_H
