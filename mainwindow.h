#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "minefield.h"
#include "settings.h"
#include "statistics.h"

#include <QMainWindow>
#include <QMessageBox>
#include <QTimer>
#include <QSettings>
#include <QDir>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Minefield *minefield;

    int fastestTimeWon = 99999;
    int time = 0;
    int rowCount = 10;
    int colCount = 15;
    int cellEdgeLenght = 50;
    int mineProbability = 10;
    int markerCount = 10;

    int winCount = 0;
    int loseCount = 0;

    bool gamePaused = false;

    QSettings *userSettings;

    QAction *settingsAct;
    QAction *statisticsAct;
    QAction *helpAct;
    QAction *closeAppAct;

    QTimer *timer;

    void setupComponents();
    void setupMenuBar(QMainWindow *mainWindow);

    void fitWindow();

    void startGame();

    void showTextBox(QString text);

    void registerScoreInSettings();

    void readUserSettings();

    void togglePause();

    QString createKeyPrefix();

private slots:
    void openSettingsWidget();
    void openHelpWidget();
    void openStatisticWidget();
    void closeApplication();
    void updateTime();
    void updateMineCount();
    void updateMarkerCount();
    void onGameWon();
    void onGameLost();
    void on_btnNewGame_clicked();
    void on_btnStartStop_clicked();
    void on_btnCloseApp_clicked();
};

#endif // MAINWINDOW_H
