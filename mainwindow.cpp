#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Mineswooper");
    minefield = new Minefield();
    ui->minefieldLayout->addWidget(minefield);
    readUserSettings();
    setupComponents();
    fitWindow();

    timer = new QTimer;
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTime()));
    timer->start(1000);

    connect(minefield, SIGNAL(mineCountChanged()), this, SLOT(updateMineCount()));
    connect(minefield, SIGNAL(markerCountChanged()), this, SLOT(updateMarkerCount()));
    connect(minefield, SIGNAL(gameWon()), this, SLOT(onGameWon()));
    connect(minefield, SIGNAL(gameLost()), this, SLOT(onGameLost()));

    minefield->startNewGame(rowCount, colCount, cellEdgeLenght, mineProbability);
}

MainWindow::~MainWindow()
{
    registerScoreInSettings();
    delete ui;
}

void MainWindow::setupComponents()
{
    setupMenuBar(this);
}

void MainWindow::setupMenuBar(QMainWindow *mainWindow)
{
    QMenu *gameMenu = menuBar()->addMenu("Spiel");

    settingsAct   = gameMenu->addAction("Einstellungen");
    statisticsAct = gameMenu->addAction("Statistiken");
    helpAct       = gameMenu->addAction("Hilfe");
    closeAppAct   = gameMenu->addAction("Beenden");

    connect(settingsAct, &QAction::triggered, this, &MainWindow::openSettingsWidget);
    connect(statisticsAct, &QAction::triggered, this, &MainWindow::openStatisticWidget);
    connect(helpAct, &QAction::triggered, this, &MainWindow::openHelpWidget);
    connect(closeAppAct, &QAction::triggered, this, &MainWindow::closeApplication);
}

void MainWindow::fitWindow()
{
    this->resize(colCount * cellEdgeLenght + cellEdgeLenght, rowCount * cellEdgeLenght + 225);
}

void MainWindow::startGame()
{
    this->time = 0;
    timer->start(1000);
    minefield->startNewGame(rowCount, colCount, cellEdgeLenght, mineProbability);
    fitWindow();
}

void MainWindow::showTextBox(QString text)
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Mineswooper");
    msgBox.setText(text);
    msgBox.exec();
}

void MainWindow::registerScoreInSettings()
{
    QString keyPrefix = createKeyPrefix();
    QString gamesWonKey = keyPrefix + "/" + "gamesWon";
    QString gamesLostKey = keyPrefix + "/" + "gamesLost";
    QString mineProbKey = keyPrefix + "/" + "mineProbability";
    QString fastestWinKey = keyPrefix + "/" + "fastestWin";

    userSettings->setValue(gamesWonKey, QString::number(winCount));
    userSettings->setValue(gamesLostKey, QString::number(loseCount));
    userSettings->setValue(mineProbKey, QString::number(mineProbability));
    userSettings->setValue(fastestWinKey, QString::number(fastestTimeWon));

    userSettings->sync();
}

void MainWindow::readUserSettings()
{
    QString keyPrefix = createKeyPrefix();
    QString gamesWonKey = keyPrefix + "/" + "gamesWon";
    QString gamesLostKey = keyPrefix + "/" + "gamesLost";
    QString fastestWinKey = keyPrefix + "/" + "fastestWin";

    userSettings = new QSettings(QDir::currentPath() + "/mineswooper.ini", QSettings::IniFormat);

    winCount = userSettings->value(gamesWonKey, "").toInt();
    loseCount = userSettings->value(gamesLostKey, "").toInt();
    fastestTimeWon = userSettings->value(fastestWinKey, "").toInt();

    if(fastestTimeWon == 0) {
        fastestTimeWon = 9999;
    }
}

void MainWindow::togglePause()
{
    if(gamePaused) {
        this->gamePaused = false;
        minefield->unpauseGame();
        timer->start(1000);

    } else {
        this->gamePaused = true;
        minefield->pauseGame();
        timer->stop();
    }
}

QString MainWindow::createKeyPrefix()
{
    QString keyPrefix;
    keyPrefix.append(QString::number(colCount))
            .append("x")
            .append(QString::number(rowCount))
            .append("_")
            .append(QString::number(mineProbability));

    return keyPrefix;
}

void MainWindow::openSettingsWidget()
{
    togglePause();
    Settings *s = new Settings;
    s->exec();

    if(!s->getUserAborted()) {
        colCount = s->getColCount();
        rowCount = s->getRowCount();
        mineProbability = s->getMineProb();

        startGame();
    }

    togglePause();
    readUserSettings();
}

void MainWindow::openHelpWidget()
{
    showTextBox("Felder werden mit der linken Maustaste aufgedeckt. Felder werden mit der rechten Maustaste markiert und Markierungen werden"
                " mit der mittleren Maustaste entfernt.");
}

void MainWindow::openStatisticWidget()
{
    Statistics *stats = new Statistics;

    QStringList groups = userSettings->childGroups();

    foreach (const QString &group, groups) {
        QString wonGames = userSettings->value(group + "/" + "gamesWon").toString();
        QString lostGames = userSettings->value(group + "/" + "gamesLost").toString();
        QString mineProb = userSettings->value(group + "/" + "mineProbability").toString().append("%");
        QString fastestWin = userSettings->value(group + "/" + "fastestWin").toString();

        fastestWin = (fastestWin == "9999" ? "-" : fastestWin);

        stats->addEntry(group.left(group.indexOf("_")), mineProb, wonGames.toInt(), lostGames.toInt(), fastestWin);
    }
    stats->exec();
}

void MainWindow::closeApplication()
{
    loseCount++;
    close();
}

void MainWindow::updateTime()
{
    ui->lcdTime->display(++time);
}

void MainWindow::updateMineCount()
{
    ui->lcdMineCount->display(minefield->getMineCount());
}

void MainWindow::updateMarkerCount()
{
    ui->lcdMarkerCount->display(minefield->getMarkerCount());
}

void MainWindow::onGameWon()
{
    timer->stop();
    if(time < fastestTimeWon) {
        fastestTimeWon = time;
    }
    winCount++;
    showTextBox("Spiel gewonnen!");
    registerScoreInSettings();
    startGame();
}

void MainWindow::onGameLost()
{
    timer->stop();
    loseCount++;
    showTextBox("Spiel verloren!");
    registerScoreInSettings();
    startGame();
}

void MainWindow::on_btnNewGame_clicked()
{
    this->time = 0;
    updateTime();
    startGame();
}

void MainWindow::on_btnStartStop_clicked()
{
    togglePause();
}

void MainWindow::on_btnCloseApp_clicked()
{
    timer->stop();
    loseCount++;
    minefield->revealBoard();
}
